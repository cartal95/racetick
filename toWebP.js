const webp=require('webp-converter');
const fs=require('fs');
const path=require('path');

const files = fs.readdirSync('imagenes')
for(let file of files) {
    const fileIn = path.join('imagenes', file)
    const fileOut = path.join('imagenes', file.replace(/\..+/, '.webp'))
    webp.cwebp(fileIn, fileOut,"-q 100", (status,error) => {
        console.log(error || status);	
    });    
}
